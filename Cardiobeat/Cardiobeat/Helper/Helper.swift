//
//  Helper.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/14/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import Foundation
import UIKit

enum DeviceType {
    case iPhone4
    case iPhone5
    case iPhone6
    case iPhone6P
}

extension UIScreen {
    
    class func screenHeight() -> CGFloat {
        return CGRectGetHeight(UIScreen.mainScreen().bounds)
    }
    
    class func screenWidth() -> CGFloat {
        return CGRectGetWidth(UIScreen.mainScreen().bounds)
    }
    
    class func screenSize() -> CGSize {
        return UIScreen.mainScreen().bounds.size
    }
    
    class func screenBounds() -> CGRect {
        return UIScreen.mainScreen().bounds
    }
    
    class func deviceType() -> DeviceType {
        var deviceType: DeviceType
        
        switch self.screenHeight() {
        case 568.0:
            deviceType = .iPhone5
        case 667.0:
            deviceType = .iPhone6
        case 736.0:
            deviceType = .iPhone6P
        default:
            deviceType = .iPhone4
        }
        
        return deviceType
    }
    
    class func noToolbarMargin() -> CGFloat {
        switch self.deviceType() {
        case .iPhone6P:
            return -20
        default:
            return -16
        }
    }
}


private let kVerdana        = "Verdana"
private let kVerdanaBold    = "Verdana-Bold"
private let kVerdanaItalic  = "Verdana-Italic"
private let kMerckFont      = "Merck-Regular"

private let regular: CGFloat    = (UIScreen.deviceType() == DeviceType.iPhone4) ? 12 : 14
private let small: CGFloat      = (UIScreen.deviceType() == DeviceType.iPhone4) ? 10 : 12
private let big: CGFloat        = (UIScreen.deviceType() == DeviceType.iPhone4) ? 14 : 16
private let title: CGFloat      = (UIScreen.deviceType() == DeviceType.iPhone4) ? 16 : 18
private let bpm: CGFloat        = {
    switch UIScreen.deviceType() {
    case .iPhone4:
        return 50
    case .iPhone5:
        return 70
    case .iPhone6:
        return 80
    case .iPhone6P:
        return 100
    }
    }()

extension UIFont {
    class func bodyRegularFont() -> UIFont {
        return UIFont(name: kVerdana, size: regular)!
    }
    
    class func bodySmallFont() -> UIFont {
        return UIFont(name: kVerdana, size: small)!
    }
    
    class func bodyBigFont() -> UIFont {
        return UIFont(name: kVerdana, size: big)!
    }
    
    class func bodyRegularBoldFont() -> UIFont {
        return UIFont(name: kVerdanaBold, size: regular)!
    }
    
    class func bodySmallBoldFont() -> UIFont {
        return UIFont(name: kVerdanaBold, size: small)!
    }
    
    class func bodyBigBoldFont() -> UIFont {
        return UIFont(name: kVerdanaBold, size: big)!
    }
    
    class func bodyRegularMerckFont() -> UIFont {
        return UIFont(name: kMerckFont, size: regular)!
    }
    
    class func titleBarFont() -> UIFont {
        return UIFont(name: kMerckFont, size: title)!
    }
    
    class func bpmFont() -> UIFont {
        return UIFont(name: kVerdana, size: bpm)!
    }
}

// MARK: UIColor Extension
extension UIColor {
    class func getColor(red red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat = 1) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
    
    class func redAppColor() -> UIColor {
        return self.getColor(red: 227, green: 3, blue: 47)
    }
    
    class func yellowAppColor() -> UIColor {
        return self.getColor(red: 250, green: 205, blue: 0)
    }
    
    class func darkGrayAppColor() -> UIColor {
        return self.getColor(red: 102, green: 102, blue: 102)
    }
    
    class func grayAppColor() -> UIColor {
        return self.getColor(red: 170, green: 170, blue: 170)
    }
    
    class func blackAppColor() -> UIColor {
        return self.getColor(red: 50, green: 50, blue: 50)
    }
    
    class func backgroundColor() -> UIColor {
        return self.getColor(red: 255, green: 255, blue: 255)
    }
    
    class func lightGrayAppColor() -> UIColor {
        return self.getColor(red: 248, green: 248, blue: 248)
    }
    
    class func darkBlueAppColor() -> UIColor {
        return self.getColor(red: 48, green: 51, blue: 66)
    }
    
    class func reportHeaderBackgroundColor() -> UIColor {
        return self.getColor(red: 221, green: 221, blue: 221)
    }
}

// MARK: UIView
extension UIView {
    class func runAfterDelay(delay: NSTimeInterval, code: () -> Void) {
        let delay = delay * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        dispatch_after(time, dispatch_get_main_queue(), {
            code()
        })
    }
}

// MARK: Layer Extensions
extension UIView {
    
    func setCornerRadius(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func addShadow(offset offset: CGSize, radius: CGFloat, color: UIColor, opacity: Float, cornerRadius: CGFloat? = nil) {
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = opacity
        self.layer.shadowColor = color.CGColor
        if let r = cornerRadius {
            self.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: r).CGPath
        }
    }
    
    func addBorder(width width: CGFloat, color: UIColor) {
        self.layer.borderWidth = width
        self.layer.borderColor = color.CGColor
        self.layer.masksToBounds = true
    }
    
    func drawStroke(width width: CGFloat, color: UIColor) {
        let path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height), cornerRadius: 0)
        let shapeLayer = CAShapeLayer ()
        shapeLayer.path = path.CGPath
        shapeLayer.fillColor = UIColor.clearColor().CGColor
        shapeLayer.strokeColor = color.CGColor
        shapeLayer.lineWidth = width
        self.layer.addSublayer(shapeLayer)
    }
    
}
