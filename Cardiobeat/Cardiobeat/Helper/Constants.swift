//
//  Constants.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/16/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import Foundation

let HEADER_HEIGHT: CGFloat = 64
let TAB_BAR_HEIGHT: CGFloat = 44

struct Notification {
    static let FINISH_MEASURE = "finishMeasure"
}

struct PostMeasureMessage {
    static let TITLE = "Message:"
    static let SHORT_CONTENT = "It is best to check the heart rate in the morning after a good night’s sleep and before getting out of bed. The normal heart rate in an adult when resting is 60 to 100 beats per minute. The heart rate is lower than 60 beats per minute for physically fit people and for those who take a drug such as a beta blockers. However for patients with hypertension and/or with associated cardiovascular risk factors the recommended heart rate is 70 beats per minute."
    static let LONG_CONTENT = "Heart rate should be measured in a sitting or lying position, when you are calm, relaxed and aren’t ill. If the heart rate is lower than 60 beats per minute it is called bradycardia. If this is the case, the heart can´t pump enough blood to the body during regular activity or during exercise. The symptoms include, dizziness, feeling faint, extreme tiredness, shortness of breath, chest pains and confusión . On the other hand, if the heart rate is higher than 100 beats per minute, it is called tachycardia. If this is the case, the heart may not efficiently pump enough blood  to the rest of the body. The symptoms include shortness of breath, lightheadedness, rapid pulse rate, heart palpitations, chest pains and fainting. If you have any alterations in your heart rate please immediately contact your doctor"
}

struct Tips {
    static let titles = ["Healthy heart",
                         "Maintain a healthy weight",
                         "Control blood pressure, cholesterol and diabetes",
                         "Keep a healthy diet",
                         "Avoid stress",
                         "Exercise",
                         "Get enough quality sleep",
                         "Don’t smoke or use tobacco"]
    static let contents = ["Heart problems may be avoided by adopting a healthy lifestyle.",
                           "Controlling your weight is important. To find out if you have the right weight, you must calculate your Body Mass Index (BMI). It is calculated by dividing the weight in Kgs by the height in M\(Character(UnicodeScalar(0178))). The BMI is a good but an imperfect guide. Measuring the waist circumference is a useful tool to measure how much of abdominal fat you have.",
                           "Hypertension is a high risk factor. This illness known as well as the “silent killer” can cause a heart attack. Optimal blood pressure is less than 120/80 mmHg. Blood cholesterol and blood sugar levels should be checked regularly and treated if the levels are high.",
                           "A healthy diet includes a low intake of salt/sodium, refined sugar foods, saturated, polyunsaturated and trans fat. It should include a diet rich in fruits, vegetables and whole grains. Eating several servings a week of certain fish such as salmon and mackerel may decrease your risk of heart attack. Alcoholic beverages should be taken in moderation.",
                           "It is known that stress produces changes in our body. When we are stressed, our muscles get tense and the heart rate increases, as well as the blood pressure and the need of oxygen which makes the heart work more.",
                           "Moderate exercise for 30-60 minutes will reduce your risk of fatal heart disease. Start slowly and gradually increase the intensity, duration and frequency of your workouts.",
                           "Most adults need 7-9 hours of sleep daily. People who do not get enough sleep have a higher blood pressure, risk of diabetes and depression and obesity.",
                           "Chemicals in tobacco can damage your heart and blood vessels, leading to the narrowing of the arteries (arterosclerosis). No amount of smoking is safe and the more you smoke the greater your risk."]
}

struct TermsAndConditions {
    static let htmlStr = "<b>CARDIOBEAT APPLICATION TERMS OF USE</b></br></br>" +
        "Merck Pte. Ltd. (“Merck”) provides this CardioBeat software application (“App”) on the Apple App Store/iTunes Store and the Google Play/Play Store. The following Terms of Use shall govern your access to the use of the App, including all texts, graphics, images, information and any other materials made available through the App and constitutes a binding legal agreement between you and Merck.</br></br>" +
        
        "YOU ACKNOWLEDGE AND AGREE THAT, BY ACCESSING OR USING THE CARDIOBEAT APP YOU HAVE INDICATED THAT YOU HAVE READ, UNDERSTAND AND AGREE TO BE BOUND BY THESE TERMS OF USE. IF YOU DO NOT AGREE TO THESE TERMS OF USE, THEN DO NOT PROCEED TO ACCESS OR USE THE CARDIOBEAT APP.</br></br>" +
        
        "<b>Modification.</b></br>" +
    "Merck reserves the sole and exclusive right to change or modify the Terms of Use set out herein at any time and without prior notice. Changes or modifications to the Terms of Use will be made available on the App and you shall be bound by the modified Terms of Use. Users shall be responsible for checking the Terms of Use for updated versions. By continuing to access or use the App after the modified Terms of Use has been posted, you are agreeing to be bound by the modified Terms of Use. If the modified Terms of Use is not acceptable to you, you shall cease using the App immediately."
    
    static let content = "<html> \n"+"<head> \n"+"<style type=\"text/css\"> \n"+"body {font-family: Verdana; font-size: 14; color: rgb(102,102,102)}\n"+"</style> \n"+"</head> \n"+"<body>\(htmlStr)</body> \n"+"</html>"
    
}

struct Measurement {
    static let scanInstructionString = "Place your index finger over the rear camera and press the button to start."
    static let disclaimerString = "Disclaimer: This app does not replace a physical examination by your doctor and there may be inaccuracies in calculating the Heart Rate."
}