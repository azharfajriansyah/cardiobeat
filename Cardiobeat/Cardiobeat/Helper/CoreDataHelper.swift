//
//  CoreDataHelper.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/28/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import UIKit
import CoreData

class CoreDataHelper: NSObject {
    
    class func getCardioDatas() -> [NSManagedObject]? {
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "CardioData")
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            let results =
                try managedContext.executeFetchRequest(fetchRequest)
            let datas = results as! [NSManagedObject]
            
            return datas
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        return nil
    }
    
    class func saveCardioData(data: CardioData) {
        //1
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let entity =  NSEntityDescription.entityForName("CardioData", inManagedObjectContext:managedContext)
        
        let cardioData = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
        
        //3
        cardioData.setValue(data.bpm, forKey: "bpm")
        cardioData.setValue(data.feeling, forKey: "feeling")
        cardioData.setValue(data.date, forKey: "date")
        
        
        appDelegate.saveContext()
    }
    
    class func removeCardioData(data: NSManagedObject) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        managedContext.deleteObject(data)
        
        appDelegate.saveContext()
    }
}
