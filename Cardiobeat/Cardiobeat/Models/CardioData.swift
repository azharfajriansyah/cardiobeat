//
//  CardioData.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/21/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import UIKit

class CardioData: NSObject {
    var bpm: Int!
    var feeling: String!
    var date: NSDate!
    
    override init() {
        super.init()
    }
    
    init(bpm: Int, feeling: String, date: NSDate) {
        self.bpm = bpm
        self.feeling = feeling
        self.date = date
        super.init()
    }
}
