//
//  MessageView.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/16/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import UIKit

class MessageView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var headerBarView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var textView: UITextView!

    override func awakeFromNib() {
        backgroundColor = UIColor(white: 0, alpha: 0.8)
        
        self.alpha = 0
        contentView.transform = CGAffineTransformMakeTranslation(0, UIScreen.screenHeight())
        contentView.layer.masksToBounds = true
        contentView.layer.cornerRadius = 5
        
        
        headerBarView.backgroundColor = UIColor.redAppColor()
        
        titleLabel.textColor = UIColor.yellowAppColor()
        titleLabel.font = UIFont.bodyRegularMerckFont()
        
        textView.font = UIFont.bodyRegularFont()
        textView.textColor = UIColor.darkGrayAppColor()
        textView.textContainerInset = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
        
        closeButton.addTarget(self, action: #selector(closeView), forControlEvents: .TouchUpInside)
    }
    
    class func instantiateFromNib() -> MessageView {
        return UINib(nibName: "MessageView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! MessageView
    }
    
    func setTitle(title: String, content: NSAttributedString) {
        titleLabel.text = title
        textView.attributedText = content
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        UIView.animateWithDuration(0.3, animations: {
            self.alpha = 1
            }) { (finished) in
                UIView.animateWithDuration(0.3, animations: {
                    self.contentView.transform = CGAffineTransformMakeTranslation(0, 0)
                    self.textView.setContentOffset(CGPointZero, animated: false)
                })
                
        }
    }
    
    func closeView() {
        UIView.animateWithDuration(0.3, animations: { 
            self.contentView.transform = CGAffineTransformMakeTranslation(0, UIScreen.screenHeight())
            }) { (finished) in
                UIView.animateWithDuration(0.3, animations: { 
                    self.alpha = 0
                    }, completion: { (finished) in
                        self.removeFromSuperview()
                })
        }
    }

}
