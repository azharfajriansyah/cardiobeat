//
//  AZRoundedButton.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/14/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//


import UIKit

class AZRoundedButton: UIButton {
    var title       : String?
    var titleColor  : UIColor?
    var bgColor     : UIColor?
    var fontType    : UIFont?
    var useShadow   : Bool = false
    var borderColor : UIColor = UIColor.clearColor()
    
    var shadowLayer : CAShapeLayer!
    
    var action      : (() -> Void)!
    
    override var highlighted: Bool {
        
        didSet {
            
            self.alpha = (highlighted) ? 0.9 : 1.0
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    convenience init(title: String) {
        self.init()
        self.configureLayout(title, titleColor: UIColor.whiteColor(), bgColor: UIColor.greenColor())
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configureLayout(title: String, titleColor: UIColor = UIColor.whiteColor(), bgColor: UIColor = UIColor.clearColor(), font: UIFont = UIFont.bodyRegularBoldFont(), useShadow: Bool = false, borderColor: UIColor = UIColor.redAppColor()) {
        self.title          = title
        self.titleColor     = titleColor
        self.bgColor        = bgColor
        self.fontType       = font
        self.useShadow      = useShadow
        self.borderColor    = borderColor
        
    }
    
    func redraw() {
        if let layer = shadowLayer {
            layer.removeFromSuperlayer()
            shadowLayer = nil
        }
        self.layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = self.bounds.size.height / 2
        
        if shadowLayer == nil {
            shadowLayer             = CAShapeLayer()
            shadowLayer.path        = UIBezierPath(roundedRect: bounds, cornerRadius: bounds.size.height/2).CGPath
            shadowLayer.fillColor   = self.bgColor!.CGColor
            
            if useShadow {
                shadowLayer.shadowColor     = UIColor.darkGrayColor().CGColor
                shadowLayer.shadowPath      = shadowLayer.path
                shadowLayer.shadowOffset    = CGSize(width: 2.0, height: 2.0)
                shadowLayer.shadowOpacity   = 0.8
                shadowLayer.shadowRadius    = 5
            }
            
            layer.insertSublayer(shadowLayer, atIndex: 0)
        }
        
        layer.borderColor = borderColor.CGColor
        layer.borderWidth = 2
        
        titleLabel?.font = self.fontType
        setTitle(title, forState: UIControlState.Normal)
        setTitleColor(titleColor, forState: UIControlState.Normal)
    }
    
}
