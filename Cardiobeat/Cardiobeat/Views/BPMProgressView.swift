//
//  BPMProgressView.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/18/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import UIKit

class BPMProgressView: UIView {
    @IBOutlet weak var percentView: UIView!
    
    @IBOutlet weak var bpmIcon: UIImageView!
    
    
    @IBOutlet weak var bpmLabel: UILabel!
    @IBOutlet weak var bpmTitle: UILabel!
    @IBOutlet weak var bpmTitleIcon: UIImageView!
    
    let bezierPath = UIBezierPath()
    var loveShape: CAShapeLayer!
    let animation = CABasicAnimation(keyPath: "strokeEnd")
    
    let inactiveTintColor = UIColor.grayAppColor().colorWithAlphaComponent(0.5)

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.alpha = 0.0
        self.backgroundColor = UIColor.clearColor()
        
        bpmIcon.image = UIImage(named: "red_icon")?.imageWithRenderingMode(.AlwaysTemplate)
        bpmIcon.tintColor = inactiveTintColor
        
        bpmLabel.textColor = UIColor.redAppColor()
        bpmLabel.font = UIFont.bpmFont()
        bpmLabel.hidden = true
        bpmLabel.alpha = 0
        
        bpmTitle.textColor = UIColor.redAppColor()
        bpmTitle.hidden = true
        bpmTitle.alpha = 0
        
        bpmTitleIcon.hidden = true
        bpmTitleIcon.alpha = 0
        
        bezierPath.moveToPoint(CGPoint(x: 17, y: 4.1))
        bezierPath.addCurveToPoint(CGPoint(x: 25, y: 0), controlPoint1: CGPoint(x: 18.6, y: 2.3), controlPoint2: CGPoint(x: 20.9, y: 0))
        bezierPath.addCurveToPoint(CGPoint(x: 34, y: 9), controlPoint1: CGPoint(x: 30, y: 0), controlPoint2: CGPoint(x: 34, y: 4))
        bezierPath.addCurveToPoint(CGPoint(x: 17.6, y: 29.2), controlPoint1: CGPoint(x: 34, y: 15), controlPoint2: CGPoint(x: 30.5, y: 18.7))
        bezierPath.addLineToPoint(CGPoint(x: 17, y: 29.7))
        bezierPath.addLineToPoint(CGPoint(x: 16.4, y: 29.2))
        bezierPath.addCurveToPoint(CGPoint(x: 0, y: 9), controlPoint1: CGPoint(x: 3.5, y: 18.7), controlPoint2: CGPoint(x: 0, y: 15))
        bezierPath.addCurveToPoint(CGPoint(x: 9, y: 0), controlPoint1: CGPoint(x: 0, y: 4), controlPoint2: CGPoint(x: 4, y: 0))
        bezierPath.addCurveToPoint(CGPoint(x: 17, y: 4.1), controlPoint1: CGPoint(x: 13.1, y: 0), controlPoint2: CGPoint(x: 15.4, y: 2.3))
        
        bezierPath.closePath()
        
//        bezierPath.moveToPoint(CGPoint(x: 33, y: 6.64))
//        bezierPath.addCurveToPoint(CGPoint(x: 46, y: 1), controlPoint1: CGPoint(x: 36.4, y: 3.14), controlPoint2: CGPoint(x: 40.97, y: 1))
//        bezierPath.addCurveToPoint(CGPoint(x: 62.49, y: 11.47), controlPoint1: CGPoint(x: 53.05, y: 1), controlPoint2: CGPoint(x: 59.21, y: 5.21))
//        bezierPath.addCurveToPoint(CGPoint(x: 65, y: 21.83), controlPoint1: CGPoint(x: 64.09, y: 14.52), controlPoint2: CGPoint(x: 65, y: 18.06))
//        bezierPath.addCurveToPoint(CGPoint(x: 46, y: 48.13), controlPoint1: CGPoint(x: 65, y: 33.33), controlPoint2: CGPoint(x: 58.52, y: 36.7))
//        bezierPath.addCurveToPoint(CGPoint(x: 33, y: 58), controlPoint1: CGPoint(x: 40, y: 53.62), controlPoint2: CGPoint(x: 33, y: 58))
//        bezierPath.addCurveToPoint(CGPoint(x: 20, y: 48.13), controlPoint1: CGPoint(x: 33, y: 58), controlPoint2: CGPoint(x: 24.31, y: 52.03))
//        bezierPath.addCurveToPoint(CGPoint(x: 1, y: 21.83), controlPoint1: CGPoint(x: 11, y: 40), controlPoint2: CGPoint(x: 1, y: 33.33))
//        bezierPath.addCurveToPoint(CGPoint(x: 3.6, y: 11.3), controlPoint1: CGPoint(x: 1, y: 17.99), controlPoint2: CGPoint(x: 1.95, y: 14.39))
//        bezierPath.addCurveToPoint(CGPoint(x: 20, y: 1), controlPoint1: CGPoint(x: 6.9, y: 5.14), controlPoint2: CGPoint(x: 13.01, y: 1))
//        bezierPath.addCurveToPoint(CGPoint(x: 33, y: 6.64), controlPoint1: CGPoint(x: 25.03, y: 1), controlPoint2: CGPoint(x: 29.6, y: 3.14))
//        bezierPath.closePath()
        
        loveShape = CAShapeLayer()
        
        loveShape.path = bezierPath.CGPath
        loveShape.lineWidth = 6
        loveShape.fillColor = UIColor.whiteColor().colorWithAlphaComponent(0.9).CGColor
        loveShape.strokeColor = self.inactiveTintColor.CGColor
        percentView.layer.addSublayer(loveShape)
    }
    
    class func instantiateFromNib() -> BPMProgressView {
        return UINib(nibName: "BPMProgressView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! BPMProgressView
    }
    
    override func didMoveToSuperview() {
        if let sv = self.superview {
            let margin: CGFloat = {
                switch UIScreen.deviceType() {
                case .iPhone4:
                    return 0
                case .iPhone5:
                    return 20
                case .iPhone6:
                    return 100
                case .iPhone6P:
                    return 140
                }
            }()
            let preferredHeight = (sv.bounds.size.height) - margin
            let preferredWidth = preferredHeight
            self.frame = CGRect(x: 0, y: 0, width: preferredWidth, height: preferredHeight)
            
            self.center = CGPoint(x: sv.bounds.size.width / 2, y: sv.bounds.size.height / 2)
            
            self.layoutIfNeeded()
            
            self.alpha = 1.0
            
            let boundingBox = CGPathGetBoundingBox(bezierPath.CGPath)
            
            let boundingBoxAspectRatio = boundingBox.width/boundingBox.height
            let viewAspectRatio: CGFloat = 1
            
            let scaleFactor: CGFloat
            if (boundingBoxAspectRatio > viewAspectRatio) {
                // Width is limiting factor
                scaleFactor = preferredWidth/boundingBox.width
            } else {
                // Height is limiting factor
                scaleFactor = preferredWidth/boundingBox.height
            }
            
            var affineTransform = CGAffineTransformMakeScale(scaleFactor, scaleFactor)
            let transformedPath = CGPathCreateCopyByTransformingPath(bezierPath.CGPath, &affineTransform)
            loveShape.path = transformedPath
            
            updateProgress(0)
        }
    }
    
    func startMeasuring() {
        iconBlinkingAnimation(true)
    }
    
    func finishMeasuring(bpm: Int) {
        UIView.animateWithDuration(0.3, animations: {
            self.bpmIcon.alpha = 0
            }) { (finished) in
                self.bpmIcon.hidden = true
                self.bpmTitleIcon.hidden = false
                self.bpmLabel.hidden = false
                self.bpmTitle.hidden = false
                UIView.animateWithDuration(0.3, animations: { 
                    self.bpmLabel.text = NSString(format: "%d", bpm) as String
                    self.bpmLabel.alpha = 1
                    self.bpmTitleIcon.alpha = 1
                    self.bpmTitle.alpha = 1
                    }, completion: { (finished) in
                        //
                })
        }
        
    }
    
    func cancelMeasuring() {
        iconBlinkingAnimation(false)
    }
    
    func iconBlinkingAnimation(start: Bool) {
        if start {
            UIView.animateWithDuration(0.8, delay: 0.0, options: [UIViewAnimationOptions.Repeat, UIViewAnimationOptions.Autoreverse], animations: {
                self.bpmIcon.tintColor = UIColor.redAppColor()
                }, completion: nil)
        } else {
            self.bpmIcon.layer.removeAllAnimations()
            UIView.animateWithDuration(0.2) {
                self.bpmIcon.tintColor = self.inactiveTintColor
            }
        }
    }
    
    func updateProgress(percent: CGFloat) {
        loveShape.strokeColor = inactiveTintColor.CGColor
        loveShape.removeAllAnimations()
        
        if percent == 0 {
            animation.toValue = 0
        } else {
            loveShape.strokeColor = UIColor.redAppColor().CGColor
            
            animation.fromValue = animation.toValue
            animation.toValue = percent
            
            animation.duration = 0
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            loveShape.addAnimation(animation, forKey: "strokeAnim")
        }
    }
    
}
