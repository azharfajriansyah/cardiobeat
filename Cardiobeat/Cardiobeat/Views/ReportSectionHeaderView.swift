//
//  ReportSectionHeaderView.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/26/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import UIKit

class ReportSectionHeaderView: UIView {
    @IBOutlet weak var heartRateTitle: UILabel!
    @IBOutlet weak var feelingTitle: UILabel!
    @IBOutlet weak var dateNTimeTitle: UILabel!
    @IBOutlet weak var dateNTimeWidthConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor.reportHeaderBackgroundColor()
        
        heartRateTitle.font = UIFont.bodySmallBoldFont()
        feelingTitle.font = UIFont.bodySmallBoldFont()
        dateNTimeTitle.font = UIFont.bodySmallBoldFont()
        
        heartRateTitle.textColor = UIColor.darkGrayAppColor()
        feelingTitle.textColor = UIColor.darkGrayAppColor()
        dateNTimeTitle.textColor = UIColor.darkGrayAppColor()
        
        if (UIScreen.deviceType() == DeviceType.iPhone4) {
            dateNTimeWidthConstraint.constant = 76
        }
    }
    
    class func instantiateFromNib() -> ReportSectionHeaderView {
        return UINib(nibName: "ReportSectionHeaderView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! ReportSectionHeaderView
    }

}
