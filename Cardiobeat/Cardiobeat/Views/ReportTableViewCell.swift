//
//  ReportTableViewCell.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/23/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import UIKit

class ReportTableViewCell: UITableViewCell {
    @IBOutlet weak var heartIcon: UIImageView!
    @IBOutlet weak var bpmLabel: UILabel!
    @IBOutlet weak var feelingLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    var data: CardioData! {
        didSet {
            configureViews()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        heartIcon.image = UIImage(named: "red_icon")!.imageWithRenderingMode(.AlwaysTemplate)
        heartIcon.tintColor = UIColor.yellowAppColor()
        
        bpmLabel.font = UIFont.bodyBigFont()
        bpmLabel.textColor = UIColor.redAppColor()
        
        feelingLabel.font = UIFont.bodySmallFont()
        feelingLabel.textColor = UIColor.darkGrayAppColor()
        
        dateLabel.font = UIFont.bodySmallFont()
        dateLabel.textColor = UIColor.darkGrayAppColor()
        
        timeLabel.font = UIFont.bodySmallFont()
        timeLabel.textColor = UIColor.darkGrayAppColor()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func nib() -> UINib {
        return UINib(nibName: "ReportTableViewCell", bundle: nil)
    }
    
    func configureViews() {
        bpmLabel.text = "\(data.bpm!) BPM"
        feelingLabel.text = data.feeling!
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM"
        
        let timeFormatter = NSDateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        
        dateLabel.text = dateFormatter.stringFromDate(data.date)
        timeLabel.text = timeFormatter.stringFromDate(data.date)
    }
    
}
