//
//  AZGraphView.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/22/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import UIKit

private let xLegendHeight: CGFloat = 30
private let yLegendWidth: CGFloat = 40
private let defaultLowest: Int = 0
private let defaultHighest: Int = 100
private let pointBarWidth: CGFloat = 60

class AZGraphView: UIView {
    @IBOutlet weak var yLegendView: UIView!
    @IBOutlet weak var yLegendViewContainer: UIView!
    @IBOutlet weak var graphContainer: UIScrollView!
    
    var graphHeight: CGFloat!
    var graphWidth: CGFloat!
    var graphView: UIView!
    var initialPoint: CGPoint!
    var highest: Int!
    var lowest: Int!
    var range: Int!
    var points = [CGPoint]()
    
    var cardioData: [CardioData]! {
        didSet {
            configureViews()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor.darkBlueAppColor()
    }
    
    class func instantiateFromNib() -> AZGraphView {
        return UINib(nibName: "AZGraphView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! AZGraphView
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        graphContainer.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height)
        
        graphHeight = self.bounds.size.height - xLegendHeight
        graphWidth = self.bounds.size.width
        graphView = UIView(frame: CGRect(x: 0, y: xLegendHeight, width: graphWidth, height: graphHeight))
        graphContainer.addSubview(graphView)
        
        yLegendViewContainer.frame = CGRect(x: 0, y: 0, width: yLegendWidth, height: self.bounds.height)
        yLegendViewContainer.backgroundColor = UIColor.darkBlueAppColor()
        yLegendViewContainer.layer.shadowColor = UIColor.blackColor().CGColor
        yLegendViewContainer.layer.shadowOpacity = 0.5
        yLegendViewContainer.layer.shadowRadius = 3.0
        yLegendViewContainer.layer.shadowOffset = CGSize(width: 1.0, height: -3.0)
        yLegendViewContainer.layer.masksToBounds = false
        yLegendViewContainer.layer.shadowPath = UIBezierPath(rect: yLegendViewContainer.frame).CGPath
        
        let initialYPoint = graphHeight / 2
        initialPoint = CGPoint(x: 0, y: initialYPoint)
        
    }
    
    func configureViews() {
        clearViews()
        
        if cardioData.count > 0 {
            let sortedData = cardioData.sort { (data0, data1) -> Bool in
                data0.bpm > data1.bpm
            }
            
            highest = (sortedData.first?.bpm)!
            lowest = (sortedData.last?.bpm)!
            
            if highest < defaultHighest { highest = defaultHighest }
            if lowest > defaultLowest { lowest = defaultLowest }
            range = highest - lowest
            
            self.drawYLegend(lowest: self.lowest, highest: self.highest)
            self.drawPlot()
        }
    }
    
    func clearViews() {
        for subView in graphView.subviews {
            subView.removeFromSuperview()
        }
        for subView in graphContainer.subviews {
            if subView != graphView {
                subView.removeFromSuperview()
            }
        }
        for subView in yLegendView.subviews {
            subView.removeFromSuperview()
        }
        
        if let layers = graphView.layer.sublayers {
            for layer in layers {
                layer.removeFromSuperlayer()
            }
        }
        
        points.removeAll()
    }
    
    func drawYLegend(lowest lowest: Int, highest: Int) {
        var legends = [Int]()
        
        var i = highest
        while i >= lowest {
            legends.append(i)
            i -= (range / 5)
        }
        
        yLegendView.frame = CGRect(x: 0, y: xLegendHeight, width: yLegendViewContainer.bounds.size.width, height: yLegendViewContainer.bounds.size.height - xLegendHeight)
        let gap = yLegendView.bounds.size.height / (CGFloat(legends.count) - 1)
        
        var multiplier: CGFloat = 0
        for legend in legends {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: yLegendWidth - 4, height: gap))
            label.font = UIFont.bodySmallBoldFont()
            label.textColor = UIColor.grayAppColor().colorWithAlphaComponent(0.5)
            label.textAlignment = NSTextAlignment.Right
            label.text = (legend == legends.last) ? "" : "\(legend)"
            label.center = CGPoint(x: yLegendWidth / 2, y: (multiplier * gap))
            yLegendView.addSubview(label)
            
            multiplier += 1
        }
    }
    
    func drawPlot() {
        var prevPoint = initialPoint
        var i: CGFloat = 0
        for data in cardioData {
            let yPos = graphHeight - (((CGFloat(data.bpm) - CGFloat(lowest)) / CGFloat(range)) * graphHeight)
            let xPos = (i * pointBarWidth) + (pointBarWidth / 2) + yLegendWidth
            let destination = CGPoint(x: xPos, y: yPos)
            points.append(destination)
            
            drawLineBetweenPoint(prevPoint, destination: destination)
            prevPoint = destination
            i += 1
        }
        
        drawPoints()
        drawBar()
        
        //measure the width
        graphView.frame = CGRect(x: 0, y: xLegendHeight, width: (CGFloat(cardioData.count) * pointBarWidth) + yLegendWidth, height: graphHeight)
        graphContainer.contentSize = CGSize(width: graphView.bounds.size.width, height: self.bounds.size.height)
    }
    
    func drawLineBetweenPoint(origin: CGPoint, destination: CGPoint) {
        var lineShape: CAShapeLayer
        var linePath: CGMutablePathRef
        
        linePath = CGPathCreateMutable()
        lineShape = CAShapeLayer()
        
        lineShape.lineWidth = 3
        lineShape.lineCap = kCALineCapRound
        lineShape.lineJoin = kCALineJoinBevel
        lineShape.strokeColor = UIColor.whiteColor().CGColor
        
        CGPathMoveToPoint(linePath, nil, origin.x, origin.y)
        CGPathAddLineToPoint(linePath, nil, destination.x, destination.y)
        
        lineShape.path = linePath
        
        graphView.layer.addSublayer(lineShape)
    }
    
    func drawPoints() {
        for point in points {
            let pointRect = CGRect(x: 0, y: 0, width: 16, height: 16)
            
            let jyPoint = JYGraphPoint(frame: pointRect)
            jyPoint.strokeColour = UIColor.whiteColor()
            jyPoint.fillColour = UIColor.darkBlueAppColor()
            jyPoint.center = point
            jyPoint.backgroundColor = UIColor.clearColor()
            graphView.addSubview(jyPoint)
        }
    }
    
    func drawBar() {
        var x = yLegendWidth
        for data in cardioData {
            let label = UILabel(frame: CGRect(x: x, y: 0, width: pointBarWidth, height: xLegendHeight))
            label.textColor = UIColor.grayAppColor().colorWithAlphaComponent(0.3)
            label.textAlignment = NSTextAlignment.Center
            label.font = UIFont.bodySmallFont()
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd-MM"
            label.text = dateFormatter.stringFromDate(data.date)
            graphContainer.addSubview(label)
            
            x += pointBarWidth
            let line = UIView(frame: CGRect(x: x, y: 0, width: 1, height: graphContainer.bounds.size.height))
            line.backgroundColor = UIColor.grayAppColor().colorWithAlphaComponent(0.1)
            graphContainer.addSubview(line)
        }
    }
}
