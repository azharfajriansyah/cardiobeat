//
//  TipsTableViewCell.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/16/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import UIKit

struct TipsCellData {
    var title: String
    var tips: String
}

typealias ShareButtonCallback = (data: TipsCellData) -> Void

class TipsTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tipsLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    
    var shareButtonCallback: ShareButtonCallback?
    
    var data: TipsCellData! {
        didSet {
            configureView()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        titleLabel.font         = UIFont.bodyBigBoldFont()
        titleLabel.textColor    = UIColor.blackAppColor()
        
        tipsLabel.font      = UIFont.bodyRegularFont()
        tipsLabel.textColor = UIColor.darkGrayAppColor()
        
        
        shareButton.setTitle("SHARE", forState: .Normal)
        shareButton.setTitleColor(UIColor.grayAppColor(), forState: .Normal)
        shareButton.titleLabel?.font = UIFont.bodyRegularFont()
        shareButton.tintColor = UIColor.grayAppColor()
        shareButton.setImage(UIImage(named: "share")?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
        shareButton.addTarget(self, action: #selector(shareButtonAction), forControlEvents: .TouchUpInside)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    class func nib() -> UINib {
        return UINib(nibName: "TipsTableViewCell", bundle: nil)
    }
    
    func configureView() {
        titleLabel.text = data.title
        tipsLabel.text = data.tips
    }
    
    func shareButtonAction() {
        if let callback = shareButtonCallback {
            callback(data: data)
        }
    }
    
    func setShareButtonCallback(callback: ShareButtonCallback) {
        shareButtonCallback = callback
    }
    
}
