//
//  BarTitleView.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/19/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import UIKit

class BarTitleView: UIView {
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var backImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor.clearColor()
        
        backImageView.image = UIImage(named: "back")?.imageWithRenderingMode(.AlwaysTemplate)
        titleImageView.image = UIImage(named: "title_icon")?.imageWithRenderingMode(.AlwaysTemplate)
        titleLabel.font = UIFont.titleBarFont()
    }
    
    class func instantiateFromNib() -> BarTitleView {
        return UINib(nibName: "BarTitleView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! BarTitleView
    }
    
    func configureViewWithTitle(title: String, tintColor: UIColor) {
        titleLabel.text = title
        titleLabel.textColor = tintColor
        titleLabel.sizeToFit()
        backImageView.tintColor = tintColor
        titleImageView.tintColor = tintColor
    }
    
    func hideBackButton() {
        backImageViewWidthConstraint.constant = 0
    }

}
