//
//  HeaderBackgroundView.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/14/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import UIKit

class HeaderBackgroundView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    class func instantiateFromNib() -> HeaderBackgroundView {
        return UINib(nibName: "HeaderBackgroundView", bundle: nil).instantiateWithOwner(nil, options: nil)[0] as! HeaderBackgroundView
    }

}
