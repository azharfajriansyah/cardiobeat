//
//  BaseViewController.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/14/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var screenName: String!
    
    lazy var headerV: HeaderBackgroundView = {
        var headerView = HeaderBackgroundView.instantiateFromNib()
        return headerView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.backgroundColor()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        sendGAScreenTracking(screenName)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupNavigationBar(tintColor: UIColor) {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.translucent = true
        
        navigationController?.navigationBar.tintColor = tintColor
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
    }
    
    func setupNavigationBarWithTitle(title: String, tintColor: UIColor, hideBackButton: Bool = true) {
        setupNavigationBar(tintColor)
        
        let titleView = BarTitleView.instantiateFromNib()
        titleView.configureViewWithTitle(title, tintColor: tintColor)
        if hideBackButton {
            titleView.hideBackButton()
        }
        
        navigationItem.titleView = titleView
        
    }
    
    func setupRightBarButtonItemWithImage(image: UIImage) {
        let barButtonItem = UIBarButtonItem(image: image, style: .Plain, target: self, action: #selector(rightBarButtonItemAction))
        navigationItem.setRightBarButtonItem(barButtonItem, animated: true)
    }
    
    func rightBarButtonItemAction() {
        
    }
    
    func setupLeftBarButtonItemWithView(view: UIView) {
        let barButtonItem = UIBarButtonItem(customView: view)
        navigationItem.setLeftBarButtonItem(barButtonItem, animated: true)
    }
    
    func leftBarButtonItemAction() {
        
    }
    
    func showMessageViewWithTitle(title: String, content: NSAttributedString) {
        let messageView = MessageView.instantiateFromNib()
        messageView.frame = CGRect(x: 0, y: 0, width: UIScreen.screenWidth(), height: UIScreen.screenHeight())
        messageView.setTitle(title, content: content)
        
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        delegate.window?.addSubview(messageView)
    }
    
    func showAlertWithTitle(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
        alertController.addAction(okAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func removeBackButtonTitle() {
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backButton
    }
    
    func showTermAndUseView() {
        let attributedText = try! NSAttributedString(data: TermsAndConditions.content.dataUsingEncoding(NSUTF8StringEncoding)!, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding], documentAttributes: nil)
        showMessageViewWithTitle("Term & Use", content: attributedText)
    }
    
    func sendGAScreenTracking(name: String) {
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: name)
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
}
