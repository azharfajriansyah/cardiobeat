//
//  YMPulseRateMater.m
//  pulseRateMeter
//
//  Created by matsumoto on 2012/12/23.
//  Copyright (c) 2012年 matsumoto. All rights reserved.
//

#import "YMPulseRateMater.h"
#import <AVFoundation/AVFoundation.h>
#import <Accelerate/Accelerate.h>

#define CALC_FRAME 256
#define INIT_FRAME 100

#define MAX_PULSE 200
#define MIN_PULSE 50

@interface YMPulseRateMater ()<AVCaptureVideoDataOutputSampleBufferDelegate>
{
    int frameCount;
    float* whitePixelCounts;
    dispatch_queue_t backgroundQueue;
}
@property (nonatomic, strong) AVCaptureSession* session;
@property (nonatomic, strong) NSDate* date;
@property (nonatomic, strong) NSDate* endDate;
@end

@implementation YMPulseRateMater

- (id)init
{
    self = [super init];
    if (self) {
        frameCount = 0;
        whitePixelCounts = calloc(CALC_FRAME, sizeof(float));
        backgroundQueue = dispatch_queue_create("com.mycompany.myqueue", 0);
    }
    return self;
}

- (void)start
{
    [self setupConnection];
    
//        [self.delegate pulseRateMeter:self completeWithPulseRate:79];
}

#pragma mark - av foundation
- (void)setupConnection
{
    
    //デバイス取得
    __strong AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    //入力作成
    __strong AVCaptureDeviceInput* deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:NULL];
    
    //ビデオデータ出力作成
    __strong NSDictionary* settings = @{(id)kCVPixelBufferPixelFormatTypeKey:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA]};
    __strong AVCaptureVideoDataOutput* dataOutput = [[AVCaptureVideoDataOutput alloc] init];
    dataOutput.videoSettings = settings;
    [dataOutput setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
    
    [device lockForConfiguration:nil];
    device.torchMode = AVCaptureTorchModeOn;
    [device unlockForConfiguration];
    
    //セッション作成
    self.session = [[AVCaptureSession alloc] init];
    [self.session addInput:deviceInput];
    [self.session addOutput:dataOutput];
    self.session.sessionPreset = AVCaptureSessionPreset352x288;
    
    __strong AVCaptureConnection *videoConnection = NULL;
    
    [self.session beginConfiguration];
    
    for ( AVCaptureConnection *connection in [dataOutput connections] )
    {
        for ( AVCaptureInputPort *port in [connection inputPorts] )
        {
            if ( [[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
                
            }
        }
    }
    if([videoConnection isVideoOrientationSupported]) // **Here it is, its always false**
    {
        [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
    }
    
    [self.session commitConfiguration];
    
    
    [self.session startRunning];
    
}

-(void)stop{
    [self.session stopRunning];
    [self.delegate didManuallyStop];
}

//各フレームにおける処理
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    // 画像の表示
    
    CVPixelBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    uint8_t *buf = (uint8_t *)CVPixelBufferGetBaseAddress(imageBuffer);
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    float r = 0, g = 0,b = 0;
    for(int y = 0; y < height; y++) {
        for(int x = 0; x < width * 4; x += 4) {
            b += buf[x];
            g += buf[x+1];
            r += buf[x+2];
        }
        buf += bytesPerRow;
    }
    r /= 255 * (float)(width * height);
    g /= 255 * (float)(width * height);
    b /= 255 * (float)(width * height);
    
    if (r > 0.6) {
        float h,s,v;
        RGBtoHSV(r, g, b, &h, &s, &v);
        static float lastH = 0;
        float highPassValue = h - lastH;
        lastH = h;
        float lastHighPassValue = 0;
        float lowPassValue = (lastHighPassValue + highPassValue) / 2;
        lastHighPassValue = highPassValue;
        
        //NSLog(@"red value: %f", r);
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate updateLowPass:lowPassValue * 1000];
        });
        
        [self calculateWithSampleBufferRef:sampleBuffer];
    } else {
        frameCount = 0;
        [self.delegate updateProgress:0];
    }
}

- (void)calculateWithSampleBufferRef:(CMSampleBufferRef)sampleBuffer
{
    
    
    
    // イメージバッファの取得
    CVImageBufferRef    buffer;
    buffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    // イメージバッファのロック
    CVPixelBufferLockBaseAddress(buffer, 0);
    // イメージバッファ情報の取得
    __block uint8_t*    base;
    __block size_t      width, height, bytesPerRow;
    base = CVPixelBufferGetBaseAddress(buffer);
    width = CVPixelBufferGetWidth(buffer);
    height = CVPixelBufferGetHeight(buffer);
    bytesPerRow = CVPixelBufferGetBytesPerRow(buffer);
    
    // ビットマップコンテキストの作成
    CGColorSpaceRef colorSpace;
    CGContextRef    cgContext;
    colorSpace = CGColorSpaceCreateDeviceRGB();
    cgContext = CGBitmapContextCreate(
                                      base, width, height, 8, bytesPerRow, colorSpace,
                                      kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGColorSpaceRelease(colorSpace);
    
    // 画像の作成
    CGImageRef cgImage;
    cgImage = CGBitmapContextCreateImage(cgContext);
    
    [self calcuratePulseRate:cgImage width:width height:height bytesPerRow:bytesPerRow];
    
    CGImageRelease(cgImage);
    CGContextRelease(cgContext);
    
    // イメージバッファのアンロック
    CVPixelBufferUnlockBaseAddress(buffer, 0);
    
}

- (void)calcuratePulseRate:(CGImageRef)cgImage width:(size_t)width height:(size_t)height bytesPerRow:(size_t)bytesPerRow
{
    
    if(frameCount > INIT_FRAME + CALC_FRAME){
        return;
    }
    frameCount++;
    
    if(frameCount < INIT_FRAME){
        [self.delegate updateProgress:(float)frameCount / (float)(INIT_FRAME + CALC_FRAME)];
    }
    
    if (frameCount == INIT_FRAME) {
//        NSLog(@"Init frame");
        self.date = [NSDate date];
        if ([self.delegate respondsToSelector:@selector(pulseRateMeterStartMeasureing:)]) {
            [self.delegate pulseRateMeterStartMeasureing:self];
        }
    }
    
    //you can use any string instead "com.mycompany.myqueue"
    
    
    if (frameCount >= INIT_FRAME && frameCount <= INIT_FRAME + CALC_FRAME) {
//        NSLog((@"Start Measure"));
        if(frameCount == INIT_FRAME + CALC_FRAME){
            self.endDate = [NSDate date];
        }
        [self countWhitePixel:cgImage width:width height:height bytesPerRow:bytesPerRow withFrameCount:frameCount withDate:[NSDate date]];
    }
    
//    frameCount++;
//    if (frameCount == INIT_FRAME) {
//        self.date = [NSDate date];
//        if ([self.delegate respondsToSelector:@selector(pulseRateMeterStartMeasureing:)]) {
//            [self.delegate pulseRateMeterStartMeasureing:self];
//        }
//    }
//    if (frameCount >= INIT_FRAME && frameCount < INIT_FRAME + CALC_FRAME) {
//        whitePixelCounts[frameCount - INIT_FRAME] = (float)([self countWhitePixel:cgImage width:width height:height bytesPerRow:bytesPerRow] / 1000000.0f);
//    }
//    if (frameCount == INIT_FRAME + CALC_FRAME) {
//        float pulse = [self getMaxPowerPulseFrom:whitePixelCounts time:[[NSDate date] timeIntervalSinceDate:self.date]];
//        [self.session stopRunning];
//        [self.delegate pulseRateMeter:self completeWithPulseRate:pulse];
//    }
}

- (void)countWhitePixel:(CGImageRef)cgImage width:(size_t)width height:(size_t)height bytesPerRow:(size_t)bytesPerRow withFrameCount:(int)fc withDate:(NSDate*)date
{
    //     NSLog([NSString stringWithFormat:@"Count pixel Main thread = %d", [NSThread isMainThread]]);
    CGDataProviderRef dataProvider = CGImageGetDataProvider(cgImage);
    CFDataRef data = CGDataProviderCopyData(dataProvider);
    UInt8* pixels = (UInt8*)CFDataGetBytePtr(data);
    
    // threshold以上の輝度を持つピクセルを計測
    dispatch_async(backgroundQueue, ^{
        int count = 0;
        for (int y = 0 ; y < height; y++){
            for (int x = 0; x < width; x++){
                UInt8* buf = pixels + y * bytesPerRow + x * 4;
                UInt8 r, g, b;
                r = *(buf + 1);
                g = *(buf + 2);
                b = *(buf + 3);
                
                float h,s,v;
                
                RGBtoHSV(r, g, b, &h, &s, &v);
                
                // simple highpass and lowpass filter
                
                static float lastH=0;
                float highPassValue=h-lastH;
                lastH=h;
                float lastHighPassValue=0;
                float lowPassValue=(lastHighPassValue+highPassValue)/2;
                
                //NSLog(@"Low pass value: %f", lowPassValue);
                
                lastHighPassValue=highPassValue;
                
                if (lowPassValue < 0) {
                    count++;
                }
            }
        }
        CFRelease(data);
        
//        return count;
        //        NSLog([NSString stringWithFormat:@"Count : %d", count]);
        [self setWhitePixel:count withFrameCount:fc withDate: date];
    });
}

-(void)setWhitePixel:(int)count withFrameCount:(int)fc withDate:(NSDate*)date{
    
//    NSLog([NSString stringWithFormat:@"frame count : %d", fc]);
    if(fc != INIT_FRAME + CALC_FRAME){
        whitePixelCounts[fc - INIT_FRAME] = (float)((float)count / 1000000.0f);
        
//        float pulse = [self getMaxPowerPulseFrom:whitePixelCounts time:[date timeIntervalSinceDate:self.date]];
//        NSLog(@"Pulse : %f ", pulse);
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate updateProgress:(float)fc / (float)(INIT_FRAME + CALC_FRAME)];
        if(fc == INIT_FRAME + CALC_FRAME){
            float pulse = [self getMaxPowerPulseFrom:whitePixelCounts time:[self.endDate timeIntervalSinceDate:self.date]];
            NSLog(@"Pulse : %f ", pulse);
            [self.session stopRunning];
            [self.delegate pulseRateMeter:self completeWithPulseRate:pulse];
            [self.delegate updatePulse:pulse];
            
        }
    });
    
}

void RGBtoHSV( float r, float g, float b, float *h, float *s, float *v ) {
    float min, max, delta;
    min = MIN( r, MIN(g, b ));
    max = MAX( r, MAX(g, b ));
    *v = max;
    delta = max - min;
    if( max != 0 )
        *s = delta / max;
    else {
        // r = g = b = 0
        *s = 0;
        *h = -1;
        return;
    }
    if( r == max )
        *h = ( g - b ) / delta;
    else if( g == max )
        *h=2+(b-r)/delta;
    else
        *h=4+(r-g)/delta;
    *h *= 60;
    if( *h < 0 )
        *h += 360;
}

#pragma mark - fourier
- (float)getMaxPowerPulseFrom:(float*)wave time:(float)time
{
    unsigned int sizeLog2 = (int)(log(CALC_FRAME)/log(2));
    unsigned int size = 1 << sizeLog2;
    
    DSPSplitComplex splitComplex;
    splitComplex.realp = calloc(size, sizeof(float));
    splitComplex.imagp = calloc(size, sizeof(float));
    
    
    FFTSetup fftSetUp = vDSP_create_fftsetup(sizeLog2 + 1, FFT_RADIX2);
    float* window = calloc(size, sizeof(float));
    float* windowedInput = calloc(size, sizeof(float));
    vDSP_hann_window(window, size, 0);
    vDSP_vmul(wave, 1, window, 1, windowedInput, 1, size);
    
    for (int i = 0; i < size; i++) {
        splitComplex.realp[i] = windowedInput[i];
        splitComplex.imagp[i] = 0.0f;
    }
    vDSP_fft_zrip(fftSetUp, &splitComplex, 1, sizeLog2 + 1, FFT_FORWARD);
    
    int start = MIN_PULSE / 60.0f * time;
    int end = MAX_PULSE / 60.0f * time;
    float max = 0;
    float maxDist = 0;
    for (int i = start; i <= end; i++) {
        float real = splitComplex.realp[i];
        float imag = splitComplex.imagp[i];
        float distance = sqrt(real*real + imag*imag);
        if (maxDist < distance) {
            max = i;
            maxDist = distance;
        }
    }
    
    float pulse = max * (60.0f / time);
    
    free(splitComplex.realp);
    free(splitComplex.imagp);
    free(window);
    free(windowedInput);
    vDSP_destroy_fftsetup(fftSetUp);
    
    return pulse;
}

@end