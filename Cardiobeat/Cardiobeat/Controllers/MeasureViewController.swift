//
//  MeasureViewController.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/14/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import UIKit
import AVFoundation
import CoreGraphics

enum MeasuringState {
    case Idle
    case Measuring
    case PostMeasuring
}

class MeasureViewController: BaseViewController, YMPulseRateMeterDelegate {
    @IBOutlet weak var headerBg: UIView!
    @IBOutlet weak var startStopButton: AZRoundedButton!
    @IBOutlet weak var cancelButton: AZRoundedButton!
    @IBOutlet weak var nextButton: AZRoundedButton!
    @IBOutlet weak var labelDisclaimer: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelMeasurementDesc: UILabel!
    @IBOutlet weak var graphView: UIView!
    @IBOutlet weak var millimeterBlock: UIImageView!
    @IBOutlet weak var labelIsMeasuring: UILabel!
    
    @IBOutlet weak var postMeasureButtonContainer: UIView!
    
    var bpmProgressView: BPMProgressView!
    var pulseRateMeter: YMPulseRateMater!
    var measuringState = MeasuringState.Idle
    var graphLastPoint: CGPoint!
    var pulseRateMeterIsMeasuring = false
    var cardioData: CardioData!
    var pulseTrend: Int! = 0 // 1 for up, 0 for neutral, -1 for down
    
    var beepSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("beep_short", ofType: "m4a")!)
    var audioPlayer: AVAudioPlayer!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: "MeasureViewController", bundle: NSBundle.mainBundle())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenName = "Measure"

        // Do any additional setup after loading the view.
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(onFinishMeasure), name: Notification.FINISH_MEASURE, object: nil)
        
        audioPlayer = try! AVAudioPlayer(contentsOfURL: beepSound)
        audioPlayer.prepareToPlay()
        
        measuringState = MeasuringState.Idle
        configureViews()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        setupViewsAsPerState()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        //stop measuring
        if measuringState == MeasuringState.Measuring {
            startStopMeasuring()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        for view in headerBg.subviews {
            view.removeFromSuperview()
        }
        
        headerV.frame = headerBg.bounds
        headerBg.addSubview(headerV)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Views
    func configureViews() {
        setupNavigationBarWithTitle("cardioBeat", tintColor: UIColor.yellowAppColor())
        setupRightBarButtonItemWithImage(UIImage(named: "question")!.imageWithRenderingMode(.AlwaysOriginal))
        navigationController?.interactivePopGestureRecognizer?.enabled = false
        
        labelTitle.font = UIFont.bodyBigBoldFont()
        labelTitle.textColor = UIColor.redAppColor()
        
        labelMeasurementDesc.text = Measurement.scanInstructionString
        labelMeasurementDesc.textColor = UIColor.darkGrayAppColor()
        labelMeasurementDesc.font = UIFont.bodyRegularFont()
        
        labelIsMeasuring.text = "Measuring..."
        labelIsMeasuring.textColor = UIColor.darkGrayAppColor()
        labelIsMeasuring.font = UIFont.bodyRegularFont()
        
        startStopButton.configureLayout("START", titleColor: UIColor.redAppColor(), bgColor: UIColor.clearColor())
        startStopButton.addTarget(self, action: #selector(startStopMeasuring), forControlEvents: .TouchUpInside)
        
        cancelButton.configureLayout("CANCEL", titleColor: UIColor.redAppColor(), bgColor: UIColor.clearColor())
        cancelButton.addTarget(self, action: #selector(cancelButtonAction), forControlEvents: .TouchUpInside)
        
        nextButton.configureLayout("NEXT", titleColor: UIColor.redAppColor(), bgColor: UIColor.clearColor())
        nextButton.addTarget(self, action: #selector(nextButtonAction), forControlEvents: .TouchUpInside)
        
        labelDisclaimer.font = UIFont.bodySmallFont()
        labelDisclaimer.textColor = UIColor.grayAppColor()
        labelDisclaimer.text = Measurement.disclaimerString
        
    }
    
    func setupViewsAsPerState() {
        UIView.animateWithDuration(0.0, animations: {
            self.labelTitle.alpha                   = 0
            self.labelMeasurementDesc.alpha         = 0
            self.labelIsMeasuring.alpha             = 1
            self.graphView.alpha                    = 0
            self.startStopButton.alpha              = 0
            self.postMeasureButtonContainer.alpha   = 0
            self.labelDisclaimer.alpha              = 1
            
            self.labelTitle.hidden                  = true
            self.labelMeasurementDesc.hidden        = true
            self.labelIsMeasuring.hidden            = true
            self.graphView.hidden                   = true
            self.startStopButton.hidden             = true
            self.postMeasureButtonContainer.hidden  = true
            self.labelDisclaimer.hidden             = false
            
            self.labelTitle.text = "Measurement"
            self.labelMeasurementDesc.text = Measurement.scanInstructionString
            
            self.pulseRateMeterIsMeasuring = false
            self.graphLastPoint = nil
            
            }) { (finished) in
                switch self.measuringState {
                case .Idle:
                    self.labelTitle.hidden = false
                    self.graphView.hidden = false
                    self.startStopButton.hidden = false
                    self.labelMeasurementDesc.hidden = false
                    self.startStopButton.configureLayout("START", titleColor: UIColor.redAppColor(), bgColor: UIColor.clearColor())
                    self.startStopButton.redraw()
                    UIView.animateWithDuration(0.1, animations: {
                        self.labelTitle.alpha = 1
                        self.labelDisclaimer.alpha = 1
                        self.graphView.alpha = 1
                        self.startStopButton.alpha = 1
                        self.labelMeasurementDesc.alpha = 1
                    })
                    
                    if self.bpmProgressView != nil && self.bpmProgressView.isDescendantOfView(self.graphView) {
                        self.bpmProgressView.removeFromSuperview()
                        self.bpmProgressView = nil
                    }
                    
                    self.bpmProgressView = BPMProgressView.instantiateFromNib()
                    self.graphView.addSubview(self.bpmProgressView)
                    
                    if let sublayers = self.millimeterBlock.layer.sublayers {
                        for sublayer in sublayers  {
                            if sublayer is CAShapeLayer { sublayer.removeFromSuperlayer() }
                        }
                    }
                
                case .Measuring:
                    self.labelTitle.hidden = false
                    self.labelIsMeasuring.hidden = false
                    self.graphView.hidden = false
                    self.startStopButton.hidden = false
                    self.startStopButton.configureLayout("STOP", titleColor: UIColor.redAppColor(), bgColor: UIColor.clearColor())
                    self.startStopButton.redraw()
                    UIView.animateWithDuration(0.1, animations: {
                        self.labelTitle.alpha = 1
                        self.graphView.alpha = 1
                        self.startStopButton.alpha = 1
                    })
                case .PostMeasuring:
                    
                    self.labelTitle.text = "Result"
                    self.labelMeasurementDesc.text = "Your result is:\n"
                    self.labelTitle.hidden = false
                    self.labelMeasurementDesc.hidden = false
                    self.graphView.hidden = false
                    self.postMeasureButtonContainer.hidden = false
                    
                    UIView.animateWithDuration(0.1, animations: { 
                        self.labelTitle.alpha = 1
                        self.labelMeasurementDesc.alpha = 1
                        self.graphView.alpha = 1
                        self.postMeasureButtonContainer.alpha = 1
                    })
                    
                }
        }
    }
    
    // MARK: - Action
    func startStopMeasuring() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            if let device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo) {
                if device.hasTorch && device.hasFlash {
                    checkCamera()
                } else {
                    showAlertWithTitle("Oops!", message: "Your device needs to have a camera and a flash.")
                }
            } else {
                showAlertWithTitle("Oops!", message: "Your device doesn't support this feature.")
            }
        } else {
            showAlertWithTitle("Oops!", message: "Your device needs to have a camera.")
            //bpmProgressView.updateProgress(100)
            //bpmProgressView.finishMeasuring(75)
            //cardioData = CardioData(bpm: 75, feeling: "", date: NSDate())
            //measuringState = .PostMeasuring
            //setupViewsAsPerState()
        }
    }
    
    func checkCamera() {
        let authStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
        switch authStatus {
        case .Authorized:
            if measuringState == MeasuringState.Measuring {
                pulseRateMeter.stop()
                bpmProgressView.cancelMeasuring()
                pulseRateMeter = nil
                measuringState = .Idle
            } else {
                pulseRateMeter = YMPulseRateMater()
                pulseRateMeter.delegate = self
                pulseRateMeter.start()
                bpmProgressView.startMeasuring()
                measuringState = .Measuring
            }
            
            setupViewsAsPerState()
        case .Denied: alertToEncourageCameraAccessInitially()
        case .NotDetermined: alertPromptToAllowCameraAccessViaSetting()
        default: alertToEncourageCameraAccessInitially()
        }
    }
    
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(
            title: "CardioBeat Would Like to Access Your Camera",
            message: nil,
            preferredStyle: UIAlertControllerStyle.Alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow", style: .Cancel, handler: { (alert) -> Void in
            UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
        }))
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {
        
        let alert = UIAlertController(
            title: "CardioBeat Would Like to Access Your Camera",
            message: nil,
            preferredStyle: UIAlertControllerStyle.Alert
        )
        alert.addAction(UIAlertAction(title: "Dismiss", style: .Cancel) { alert in
            if AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo).count > 0 {
                AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo) { granted in
                    dispatch_async(dispatch_get_main_queue()) {
                        self.checkCamera() } }
            }
            }
        )
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func onFinishMeasure() {
        cardioData = nil
        measuringState = .Idle
        setupViewsAsPerState()
    }
    
    func cancelButtonAction() {
        onFinishMeasure()
    }
    
    func nextButtonAction() {
        let postMeasureView = PostMeasureViewController(cardioData: cardioData)
        removeBackButtonTitle()
        navigationController?.pushViewController(postMeasureView, animated: true)
    }
    
    override func rightBarButtonItemAction() {
        showTermAndUseView()
    }
    
    
    // MARK: - Pulse Rate Meter
    func pulseRateMeterStartMeasureing(sender: AnyObject!) {
        print("Start Measuring")
        
        pulseRateMeterIsMeasuring = true
    }
    
    func pulseRateMeter(sender: AnyObject!, completeWithPulseRate pulseRate: Float) {
        print("Complete measuring with pulse rate: %@", pulseRate)
        measuringState = .PostMeasuring
        bpmProgressView.finishMeasuring(Int(pulseRate))
        setupViewsAsPerState()
        
        cardioData = CardioData(bpm: Int(pulseRate), feeling: "", date: NSDate())
    }
    
    func updatePulse(pulse: Float) {
        //print("Updating pulse: %@", pulse)
    }
    
    func updateProgress(progress: Float) {
        //print("Progress: %@", progress)
        bpmProgressView.updateProgress(CGFloat(progress))
    }
    
    func didManuallyStop() {
        bpmProgressView.cancelMeasuring()
        bpmProgressView.updateProgress(0)
    }
    
    func updateLowPass(lowPass: Float) {
        let height = millimeterBlock.bounds.size.height
        let medianY = height / 2
        if graphLastPoint == nil {
            graphLastPoint = CGPoint(x: 0, y: height / 2)
            pulseTrend = 0
        }
        
        var value: Float;
        if lowPass > 50 { value = 50 }
        else if lowPass < -50 { value = -50 }
        else { value = lowPass }
        
        let yPos = (height / 2) + ((value > 5 || value < -5) ? CGFloat(value) : 0)
        let xPos = graphLastPoint.x + 1.5
        let destination = CGPoint(x: xPos, y: yPos)
        
        if (pulseRateMeterIsMeasuring) {
            drawLineBetweenPoint(graphLastPoint, destination: destination)
            graphLastPoint = destination
        }
        
        if (pulseTrend == 0 || pulseTrend == 1) && (medianY - 5) > yPos && pulseRateMeterIsMeasuring {
            //let systemSoundID: SystemSoundID = 1057
            //AudioServicesPlaySystemSound (systemSoundID)
            
            let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
            dispatch_async(dispatch_get_global_queue(priority, 0)) {
                self.audioPlayer.play()
            }
        }
        
        if medianY > yPos {
            pulseTrend = -1
        } else {
            pulseTrend = 1
        }
    }
    
    func drawLineBetweenPoint(origin: CGPoint, destination: CGPoint) {
        var lineShape: CAShapeLayer
        var linePath: CGMutablePathRef
        
        linePath = CGPathCreateMutable()
        lineShape = CAShapeLayer()
        
        lineShape.lineWidth = 1
        lineShape.lineCap = kCALineCapRound
        lineShape.lineJoin = kCALineJoinBevel
        lineShape.strokeColor = UIColor.redAppColor().CGColor
        
        CGPathMoveToPoint(linePath, nil, origin.x, origin.y)
        CGPathAddLineToPoint(linePath, nil, destination.x, destination.y)
        
        lineShape.path = linePath
        
        millimeterBlock.layer.addSublayer(lineShape)
    }
}
