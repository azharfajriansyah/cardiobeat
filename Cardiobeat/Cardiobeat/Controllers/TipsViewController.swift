//
//  TipsViewController.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/14/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import UIKit
import MessageUI

class TipsViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    @IBOutlet weak var headerBg: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    let tipsCellIdentifier = "tipsCellIdentifier"
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: "TipsViewController", bundle: NSBundle.mainBundle())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenName = "Tips"

        // Do any additional setup after loading the view.
        configureViews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        for view in headerBg.subviews {
            view.removeFromSuperview()
        }
        
        headerV.frame = headerBg.bounds
        headerBg.addSubview(headerV)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Views
    func configureViews() {
        setupNavigationBarWithTitle("Tips", tintColor: UIColor.yellowAppColor())
        setupRightBarButtonItemWithImage(UIImage(named: "question")!.imageWithRenderingMode(.AlwaysOriginal))
        
        tableView.registerNib(TipsTableViewCell.nib(), forCellReuseIdentifier: tipsCellIdentifier)
    }
    
    // MARK: - Actions
    override func rightBarButtonItemAction() {
        showTermAndUseView()
    }
    
    // MARK: - TableView DataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Tips.titles.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(tipsCellIdentifier, forIndexPath: indexPath) as! TipsTableViewCell
        
        let data = TipsCellData(title: Tips.titles[indexPath.row], tips: Tips.contents[indexPath.row])
        cell.data = data
        
        cell.setShareButtonCallback { (data) in
            let alertController = UIAlertController(title: "Share the Tips", message: "Choose your preferred share action.", preferredStyle: UIAlertControllerStyle.ActionSheet)
            let emailAction = UIAlertAction(title: "Email", style: .Default, handler: { (alertAction) in
                //email action
                let mailVC = MFMailComposeViewController()
                mailVC.mailComposeDelegate = self
                mailVC.setSubject(data.title)
                mailVC.setMessageBody(data.tips, isHTML: false)
                
                self.presentViewController(mailVC, animated: true, completion: nil)
            })
            let smsAction = UIAlertAction(title: "SMS", style: .Default, handler: { (alertAction) in
                //sms action
                if MFMessageComposeViewController.canSendText() {
                    let messageVC = MFMessageComposeViewController()
                    messageVC.messageComposeDelegate = self
                    messageVC.body = data.title + " - " + data.tips
                    
                    self.presentViewController(messageVC, animated: true, completion: nil)
                } else {
                    let alertController = UIAlertController(title: "Ooops!", message: "Your device doestn't support SMS.", preferredStyle: UIAlertControllerStyle.Alert)
                    let okAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                    alertController.addAction(okAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            
            alertController.addAction(emailAction)
            alertController.addAction(smsAction)
            alertController.addAction(cancelAction)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
        return cell
    }
    
    // MARK: - TableView Delegate
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 105
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // MARK: - Mail Compose Delegate
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Message Compose Delegate
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }

}
