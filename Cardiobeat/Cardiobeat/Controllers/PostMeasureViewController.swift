//
//  PostMeasureViewController.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/27/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import UIKit

class PostMeasureViewController: BaseViewController {
    
    @IBOutlet weak var headerBg: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var normalButton: AZRoundedButton!
    @IBOutlet weak var agitatedButton: AZRoundedButton!
    @IBOutlet weak var restedButton: AZRoundedButton!
    
    @IBOutlet weak var cancelButton: AZRoundedButton!
    @IBOutlet weak var saveButton: AZRoundedButton!
    
    @IBOutlet weak var messageViewContainer: UIView!
    @IBOutlet weak var messageTitleLabel: UILabel!
    @IBOutlet weak var messageContentLabel: UILabel!
    @IBOutlet weak var seeMoreButton: UIButton!
    
    var cardioData: CardioData!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: "PostMeasureViewController", bundle: NSBundle.mainBundle())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(cardioData: CardioData) {
        self.init()
        
        self.cardioData = cardioData
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenName = "Set Feeling"

        // Do any additional setup after loading the view.
        configureViews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        for view in headerBg.subviews {
            view.removeFromSuperview()
        }
        
        headerV.frame = headerBg.bounds
        headerBg.addSubview(headerV)
    }
    
    func configureViews() {
        let titleView = BarTitleView.instantiateFromNib()
        titleView.configureViewWithTitle("cardioBeat", tintColor: UIColor.yellowAppColor())
        
        var frame = titleView.frame
        frame.size.width = UIScreen.screenWidth() * 0.75
        titleView.frame = frame
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(leftBarButtonItemAction))
        titleView.addGestureRecognizer(tapGestureRecognizer)
        titleView.userInteractionEnabled = true
        
        setupLeftBarButtonItemWithView(titleView)
        setupRightBarButtonItemWithImage(UIImage(named: "question")!.imageWithRenderingMode(.AlwaysOriginal))
        
        titleLabel.text = "How are you feeling?"
        titleLabel.font = UIFont.bodyBigBoldFont()
        titleLabel.textColor = UIColor.blackAppColor()
        
        setFeelingButtonDefaultAppearance()
        
        normalButton.addTarget(self, action:#selector(feelingButtonOnTapped(_:)), forControlEvents: .TouchUpInside)
        
        agitatedButton.addTarget(self, action:#selector(feelingButtonOnTapped(_:)), forControlEvents: .TouchUpInside)
        
        restedButton.addTarget(self, action:#selector(feelingButtonOnTapped(_:)), forControlEvents: .TouchUpInside)
        
        cancelButton.configureLayout("CANCEL", titleColor: UIColor.grayAppColor(), bgColor: UIColor.whiteColor(), font: UIFont.bodyRegularBoldFont(), useShadow: false, borderColor: UIColor.grayAppColor())
        cancelButton.addTarget(self, action: #selector(onCancel), forControlEvents: .TouchUpInside)
        saveButton.configureLayout("SAVE", titleColor: UIColor.redAppColor(), bgColor: UIColor.whiteColor(), font: UIFont.bodyRegularBoldFont(), useShadow: false, borderColor: UIColor.redAppColor())
        saveButton.addTarget(self, action: #selector(onSave), forControlEvents: .TouchUpInside)
        
        messageViewContainer.layer.masksToBounds = true
        messageViewContainer.layer.cornerRadius = 5
        messageViewContainer.layer.borderColor = UIColor.lightGrayAppColor().CGColor
        messageViewContainer.layer.borderWidth = 1
        messageTitleLabel.font = UIFont.bodySmallBoldFont()
        messageTitleLabel.textColor = UIColor.blackAppColor()
        messageTitleLabel.text = "Message:"
        
        messageContentLabel.font = UIFont.bodySmallFont()
        messageContentLabel.textColor = UIColor.grayAppColor()
        messageContentLabel.text = PostMeasureMessage.SHORT_CONTENT
        
        seeMoreButton.titleLabel?.font = UIFont.bodySmallBoldFont()
        seeMoreButton.setTitleColor(UIColor.yellowAppColor(), forState: UIControlState.Normal)
        seeMoreButton.addTarget(self, action: #selector(onSeeMoreAction), forControlEvents: .TouchUpInside)
    }
    
    func setFeelingButtonDefaultAppearance() {
        normalButton.configureLayout("NORMAL", titleColor: UIColor.grayAppColor(), bgColor: UIColor.whiteColor(), font: UIFont.bodyRegularBoldFont(), useShadow: false, borderColor: UIColor.grayAppColor())
        normalButton.redraw()
        
        agitatedButton.configureLayout("AGITATED", titleColor: UIColor.grayAppColor(), bgColor: UIColor.whiteColor(), font: UIFont.bodyRegularBoldFont(), useShadow: false, borderColor: UIColor.grayAppColor())
        agitatedButton.redraw()
        
        restedButton.configureLayout("RESTED", titleColor: UIColor.grayAppColor(), bgColor: UIColor.whiteColor(), font: UIFont.bodyRegularBoldFont(), useShadow: false, borderColor: UIColor.grayAppColor())
        restedButton.redraw()
    }
    
    // MARK: - Actions
    override func rightBarButtonItemAction() {
        showTermAndUseView()
    }
    
    override func leftBarButtonItemAction() {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func feelingButtonOnTapped(sender: AZRoundedButton) {
        setFeelingButtonDefaultAppearance()
        
        switch sender {
        case normalButton:
            cardioData.feeling = "Normal"
        case agitatedButton:
            cardioData.feeling = "Agitated"
        case restedButton:
            cardioData.feeling = "Rested"
        default:
            break
        }
        
        sender.configureLayout(sender.title!, titleColor: UIColor.whiteColor(), bgColor: UIColor.redAppColor(), font: UIFont.bodyRegularBoldFont(), useShadow: false, borderColor: UIColor.redAppColor())
        sender.redraw()
        
        print(cardioData)
    }
    
    func onCancel() {
        NSNotificationCenter.defaultCenter().postNotificationName(Notification.FINISH_MEASURE, object: nil)
        navigationController?.popViewControllerAnimated(true)
    }
    
    func onSave() {
        if cardioData.feeling == "" {
            showAlertWithTitle("Oops", message: "Please select your feeling.")
        } else {
            CoreDataHelper.saveCardioData(cardioData)
            NSNotificationCenter.defaultCenter().postNotificationName(Notification.FINISH_MEASURE, object: nil)
            navigationController?.popViewControllerAnimated(true)
        }
    }
    
    func onSeeMoreAction() {
        let attributedText = NSAttributedString(string: PostMeasureMessage.LONG_CONTENT, attributes: [NSFontAttributeName : UIFont.bodyRegularFont(), NSForegroundColorAttributeName : UIColor.darkGrayAppColor()])
        showMessageViewWithTitle("Message", content: attributedText)
    }

}
