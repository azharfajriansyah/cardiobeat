//
//  ReportViewController.swift
//  Cardiobeat
//
//  Created by Azhar Fajriansyah on 4/14/16.
//  Copyright © 2016 Zahr Labs. All rights reserved.
//

import UIKit
import CoreData
import MessageUI

class ReportViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    let reportCellIdentifier = "reportCellIdentifier"
    
    @IBOutlet weak var headerBg: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var cardioManagedObjects: [NSManagedObject]?
    var cardioDatas: [CardioData]?
    
    lazy var graphView: AZGraphView = {
        let graphView = AZGraphView.instantiateFromNib()
        return graphView
    }()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: "ReportViewController", bundle: NSBundle.mainBundle())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        screenName = "History"

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        populateData()
        tableView.reloadData()
        
        configureViews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        for view in headerBg.subviews {
            view.removeFromSuperview()
        }
        
        headerV.frame = headerBg.bounds
        headerBg.addSubview(headerV)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func populateData() {
        if let datas = CoreDataHelper.getCardioDatas() where datas.count > 0 {
            cardioManagedObjects = datas
            
            cardioDatas = [CardioData]()
            for data in cardioManagedObjects! {
                let cardioData = CardioData(bpm: Int(data.valueForKey("bpm") as! NSNumber), feeling: String(data.valueForKey("feeling") as! String), date: data.valueForKey("date") as! NSDate)
                cardioDatas?.append(cardioData)
            }
        } else {
            cardioManagedObjects?.removeAll()
            cardioDatas?.removeAll()
        }
    }
    

    // MARK: - Views
    func configureViews() {
        setupNavigationBarWithTitle("History", tintColor: UIColor.yellowAppColor())
        setupRightBarButtonItemWithImage((UIImage(named: "message")?.imageWithRenderingMode(.AlwaysOriginal))!)
        
        tableView.registerNib(ReportTableViewCell.nib(), forCellReuseIdentifier: reportCellIdentifier)
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.screenWidth(), height: (UIScreen.screenHeight() / 2) - HEADER_HEIGHT - TAB_BAR_HEIGHT))
        
        graphView.frame = headerView.bounds
        headerView.addSubview(graphView)
        tableView.tableHeaderView = headerView
        
        setupGraphView()
    }
    
    func setupGraphView() {
        if cardioDatas?.count > 0 {
            let sortedData = cardioDatas!.sort { (data0, data1) -> Bool in
                data0.date.compare(data1.date) == NSComparisonResult.OrderedAscending
            }
            graphView.cardioData = sortedData
        } else {
            if let _ = graphView.cardioData {
                graphView.cardioData.removeAll()
            }
        }
    }
    
    // MARK: - TableView DataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let data = self.cardioDatas where data.count > 0 {
            return data.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(reportCellIdentifier, forIndexPath: indexPath) as! ReportTableViewCell
        cell.data = cardioDatas![indexPath.row]
        if indexPath.row % 2 == 0 { cell.backgroundColor = UIColor.lightGrayAppColor() }
        else { cell.backgroundColor = UIColor.whiteColor() }
        return cell
    }
    
    // MARK: - TableView Delegate
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = ReportSectionHeaderView.instantiateFromNib()
        headerView.frame = CGRect(x: 0, y: 0, width: UIScreen.screenWidth(), height: 44)
        return headerView
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            CoreDataHelper.removeCardioData(cardioManagedObjects![indexPath.row])
            populateData()
            if let datas = cardioDatas where datas.count > 0 {
                tableView.beginUpdates()
                setupGraphView()
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                tableView.endUpdates()
            } else {
                tableView.reloadData()
                setupGraphView()
            }
        }
    }
    
    // MARK: - Actions
    override func rightBarButtonItemAction() {
        
        var dataStr = ""
        if let datas = cardioDatas {
            for data in datas {
                dataStr = dataStr + "\(data.date), BPM: \(data.bpm), Feeling: \(data.feeling)\n"
            }
            
            let alertController = UIAlertController(title: "Share the history", message: "Choose your preferred share action.", preferredStyle: UIAlertControllerStyle.ActionSheet)
            let emailAction = UIAlertAction(title: "Email", style: .Default, handler: { (alertAction) in
                //email action
                let mailVC = MFMailComposeViewController()
                mailVC.mailComposeDelegate = self
                mailVC.setSubject("Cardiobeat Result History")
                mailVC.setMessageBody(dataStr, isHTML: false)
                
                self.presentViewController(mailVC, animated: true, completion: nil)
            })
            let smsAction = UIAlertAction(title: "SMS", style: .Default, handler: { (alertAction) in
                //sms action
                if MFMessageComposeViewController.canSendText() {
                    let messageVC = MFMessageComposeViewController()
                    messageVC.messageComposeDelegate = self
                    messageVC.body = "Cardiobeat Result History\n" + dataStr
                    
                    self.presentViewController(messageVC, animated: true, completion: nil)
                } else {
                    let alertController = UIAlertController(title: "Ooops!", message: "Your device doestn't support SMS.", preferredStyle: UIAlertControllerStyle.Alert)
                    let okAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                    alertController.addAction(okAction)
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            
            alertController.addAction(emailAction)
            alertController.addAction(smsAction)
            alertController.addAction(cancelAction)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            showAlertWithTitle("Oops!", message: "You have no data to be shared.")
        }
        
    }
    
    // MARK: - Mail Compose Delegate
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Message Compose Delegate
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }

}
